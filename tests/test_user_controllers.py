import uuid


class TestUserListController:
    def test_get_empty_list_of_users(self, flask_client):
        get_list_response = flask_client.get('/users')
        assert get_list_response.status_code == 200
        assert get_list_response.json == []

    def test_create_a_user(self, flask_client):
        test_email = f'testuser{uuid.uuid4()}@example.com'
        create_user_response = flask_client.post('/users', json={'email': test_email})
        assert create_user_response.status_code == 201
        user_id = create_user_response.json['id']
        user_details_response = flask_client.get(f'/users/{user_id}')
        assert user_details_response.status_code == 200
        assert user_details_response.json['email'] == test_email


class TestUserDetailsController:
    def test_not_existing_user_details_should_respond_with_error(self, flask_client):
        get_non_existing_user_details_response = flask_client.get('/users/nothere123')
        assert get_non_existing_user_details_response.status_code == 404
        assert get_non_existing_user_details_response.json['message'] == 'User with ID nothere123 does not exist'
