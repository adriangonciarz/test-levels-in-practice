import random

import requests
from faker import Faker
from locust import HttpUser, task
from locust import events

fake = Faker()

user_ids = []


@events.test_start.add_listener
def on_test_start(environment, **kwargs):
    for _ in range(100):
        payload = {'email': fake.safe_email()}
        user_create_response = requests.post('http://localhost:5000/users', json=payload)
        user_ids.append(user_create_response.json()['id'])
    print('Created user IDs', user_ids)


class AdminUser(HttpUser):
    @task(5)
    def get_list_of_users(self):
        """Getting a list of all users from /users"""
        self.client.get('/users')

    @task
    def get_details_of_a_user(self):
        user_id = random.choice(user_ids)
        self.client.get(f'/users/{user_id}', name='/users/$userId')
