import uuid

import pytest

from src.models.users import User, InvalidEmailException


@pytest.fixture
def random_email(faker):
    return faker.ascii_safe_email()


@pytest.fixture
def user(random_email):
    user = User(random_email)
    return user


class TestUserModel:
    def test_initial_user_model_data(self, random_email, user):
        # Then
        assert user.email == random_email
        assert uuid.UUID(user.user_id)

    def test_user_model_serialization_to_json(self, random_email, user):
        # When
        user_json = user.to_dict()
        # Then
        assert user_json == {
            'email': random_email,
            'id': user.user_id
        }

    def test_throwing_exception_when_email_does_not_contain_at_sign(self):
        with pytest.raises(InvalidEmailException,  match=r"Invalid email!"):
            User('abcexample.com')
