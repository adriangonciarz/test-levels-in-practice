import os

import pytest
import requests
from pact import Consumer, Provider

PACT_MOCK_HOST = 'localhost'
PACT_MOCK_PORT = 1234
PACT_DIR = os.path.dirname(os.path.realpath(__file__))


class UsersConsumer:
    def __init__(self, base_uri):
        self.base_uri = base_uri

    def get_users(self):
        uri = f'{self.base_uri}/users'
        return requests.get(uri)


@pytest.fixture
def consumer():
    return UsersConsumer(f'http://{PACT_MOCK_HOST}:{PACT_MOCK_PORT}')


@pytest.fixture(scope='session')
def pact():
    pact = Consumer('UsersConsumer').has_pact_with(
        Provider('UsersProvider'), host_name=PACT_MOCK_HOST, port=PACT_MOCK_PORT,
        pact_dir="./pacts", log_dir="./logs")
    pact.start_service()
    yield pact
    pact.stop_service()


def test_get_users(pact, consumer):
    pact.given(
        'users exist'
    ).upon_receiving(
        'a request for users list'
    ).with_request(
        'GET', '/users'
    ).will_respond_with(200)

    with pact:
        assert consumer.get_users().status_code == 200
