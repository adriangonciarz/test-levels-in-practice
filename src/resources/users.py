import random
import time

from flask import request
from flask_restful import Resource

import config
from models.users import User
from storage import UserStorage
from prometheus_client import Summary, Counter

users_storage = UserStorage

REQUEST_TIME = Summary('user_creation_seconds', 'Time spent on user creation')
user_creation_counter = Counter('user_creation', 'User creation counter')
user_creation_success_counter = Counter('user_creation_2xx', 'User success counter')


def get_user_or_throw_exception(user_id):
    user = users_storage.find(user_id)
    if not user:
        return {f"message": f"User with ID {user_id} does not exist"}, 404
    return user


class UserListController(Resource):
    # from connectors import KafkaConnector
    # kafka_connector = KafkaConnector

    def get(self):
        """Returns a list of all users"""
        return users_storage.to_list(), 200

    @REQUEST_TIME.time()
    def post(self):
        """Creates new user"""
        user_creation_counter.inc()
        time.sleep(random.random())
        user_email = request.json['email']
        existing_user = users_storage.find_by_email(user_email)
        if existing_user:
            return {f"message": f"User with email {user_email} already exist"}, 400
        new_user = User(user_email)
        users_storage.add(new_user)
        user_creation_success_counter.inc()
        # self.kafka_connector.send_to_topic(config.kafka_topic_users_updates, new_user.to_dict(), new_user.user_id)
        return new_user.to_dict(), 201


class UserController(Resource):
    def get(self, user_id):
        """Returns details of a user"""
        user = get_user_or_throw_exception(user_id)
        return user.to_dict(), 200

    def put(self, user_id):
        """Updates details of user"""
        user = get_user_or_throw_exception(user_id)
        user.email = request.json['email']
        return user.to_dict(), 201

    def delete(self, user_id):
        user = get_user_or_throw_exception(user_id)
        users_storage.remove(user)
        return {"message": f"User {user_id} deleted successfully"}, 200
