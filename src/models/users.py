import uuid
from typing import Dict


class InvalidEmailException(Exception):
    pass


class User:
    def __init__(self, email: str):
        self.__validate_email(email)
        self.email = email
        self.__user_id = str(uuid.uuid4())

    @property
    def user_id(self):
        return self.__user_id

    def to_dict(self) -> Dict:
        return {
            'email': self.email,
            'id': self.__user_id
        }

    def __validate_email(self, email: str):
        if '@' not in email:
            raise InvalidEmailException('Invalid email!')
